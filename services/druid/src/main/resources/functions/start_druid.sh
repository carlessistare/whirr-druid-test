function start_druid() {

    ROLE=$1
    echo "Inside start_druid(), ROLE=$ROLE"

    # Make logs directory
    echo "Creating log directory..."
    if [ ! -d "/usr/local/druid-services-0.6.52/logs" ]; then
      mkdir /usr/local/druid-services-0.6.52/logs
    fi

    # Start the appropriate role
    echo "Executing druid $ROLE..."
    case $ROLE in
        druid)
            # Run the realtime node
            nohup java -Xmx256m -Duser.timezone=UTC -Dfile.encoding=UTF-8 -Ddruid.realtime.specFile=/usr/local/druid-services-0.6.52/config/realtime/realtime.spec -classpath /usr/local/druid-services-0.6.52/lib/*:/usr/local/druid-services-0.6.52/config/realtime io.druid.cli.Main server realtime 2>&1 > /usr/local/druid-services-0.6.52/logs/realtime.log &

            # And a master node
            nohup java -Xmx256m -Duser.timezone=UTC -Dfile.encoding=UTF-8 -classpath /usr/local/druid-services-0.6.52/lib/*:/usr/local/druid-services-0.6.52/config/master io.druid.cli.Main server master 2>&1 > /usr/local/druid-services-0.6.52/logs/master.log &

            # And a compute node
            nohup java -Xmx256m -Duser.timezone=UTC -Dfile.encoding=UTF-8 -classpath /usr/local/druid-services-0.6.52/lib/*:/usr/local/druid-services-0.6.52/config/compute io.druid.cli.Main server compute 2>&1 > /usr/local/druid-services-0.6.52/logs/compute.log &

            # And a broker node
            nohup java -Xmx256m -Duser.timezone=UTC -Dfile.encoding=UTF-8 -classpath /usr/local/druid-services-0.6.52/lib/*:/usr/local/druid-services-0.6.52/config/broker io.druid.cli.Main server broker 2>&1 > /usr/local/druid-services-0.6.52/logs/broker.log &

            ;;
        druid-broker)
            # Run the broker node
            nohup java -Xmx256m -Duser.timezone=UTC -Dfile.encoding=UTF-8 -classpath /usr/local/druid-services-0.6.52/lib/*:/usr/local/druid-services-0.6.52/config/broker io.druid.cli.Main server broker 2>&1 > /usr/local/druid-services-0.6.52/logs/broker.log &
            ;;
        druid-master)
            # Run the master node
            nohup java -Xmx256m -Duser.timezone=UTC -Dfile.encoding=UTF-8 -classpath /usr/local/druid-services-0.6.52/lib/*:/usr/local/druid-services-0.6.52/config/master io.druid.cli.Main server master 2>&1 > /usr/local/druid-services-0.6.52/logs/master.log &
            ;;
        druid-compute)
            # Run the compute node
            nohup java -Xmx256m -Duser.timezone=UTC -Dfile.encoding=UTF-8 -classpath /usr/local/druid-services-0.6.52/lib/*:/usr/local/druid-services-0.6.52/config/compute io.druid.cli.Main server compute 2>&1 > /usr/local/druid-services-0.6.52/logs/compute.log &
            ;;
        druid-realtime)
            # Run the realtime node
            nohup java -Xmx256m -Duser.timezone=UTC -Dfile.encoding=UTF-8 -Ddruid.realtime.specFile=/usr/local/druid-services-0.6.52/config/realtime/realtime.spec -classpath /usr/local/druid-services-0.6.52/lib/*:/usr/local/druid-services-0.6.52/config/realtime io.druid.cli.Main server realtime 2>&1 > /usr/local/druid-services-0.6.52/logs/realtime.log &
            ;;
        druid-mysql)
            # Noop - MySQL runs automatically after apt-get installed
            ;;
        *)
            echo $"Usage: $0 {druid|druid-broker|druid-master|druid-compute|druid-realtime|druid-mysql}"
            exit 1
    esac

}
