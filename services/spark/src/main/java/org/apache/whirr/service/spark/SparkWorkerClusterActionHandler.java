package org.apache.whirr.service.spark;

import static org.apache.whirr.RolePredicates.role;
import static org.jclouds.scriptbuilder.domain.Statements.call;

import java.io.IOException;

import org.apache.whirr.Cluster;
import org.apache.whirr.Cluster.Instance;
import org.apache.whirr.service.ClusterActionEvent;
import org.apache.whirr.service.ClusterActionHandlerSupport;

public class SparkWorkerClusterActionHandler extends ClusterActionHandlerSupport {

	public static final String ROLE = "spark-worker";

	@Override
	public String getRole() {
		return ROLE;
	}

	@Override
	protected void beforeBootstrap(ClusterActionEvent event)
			throws IOException, InterruptedException {

		addStatement(event, call("retry_helpers"));
		addStatement(event, call("install_tarball"));
		addStatement(event, call("install_openjdk"));
		addStatement(event, call("install_scala"));
		addStatement(event, call("install_spark"));

	}

	@Override
	protected void beforeConfigure(ClusterActionEvent event)
			throws IOException, InterruptedException {
		
		Cluster cluster = event.getCluster();
		Instance master = cluster.getInstanceMatching(role(SparkMasterClusterActionHandler.ROLE));
		String masterAddress = master.getPrivateAddress().getHostAddress();
		addStatement(event, call("configure_spark_worker", masterAddress));
	}

}
