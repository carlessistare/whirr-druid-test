package org.apache.whirr.service.spark;

import static org.jclouds.scriptbuilder.domain.Statements.call;
import static org.apache.whirr.RolePredicates.role;

import java.io.IOException;

import org.apache.whirr.Cluster;
import org.apache.whirr.Cluster.Instance;
import org.apache.whirr.service.ClusterActionEvent;
import org.apache.whirr.service.ClusterActionHandlerSupport;
import org.apache.whirr.service.FirewallManager.Rule;


public class SparkMasterClusterActionHandler extends ClusterActionHandlerSupport {
	
	public static final String ROLE = "spark-master";
	public static final int LISTEN_PORT = 7077;
	public static final int WEB_UI_PORT = 8080;

	@Override public String getRole() { return ROLE; }

	@Override
	protected void beforeBootstrap(ClusterActionEvent event)
			throws IOException, InterruptedException {
		
		addStatement(event, call("retry_helpers"));
		addStatement(event, call("install_tarball"));
		addStatement(event, call("install_openjdk"));
		addStatement(event, call("install_scala"));
		addStatement(event, call("install_spark"));
	
	}

	@Override
	protected void beforeConfigure(ClusterActionEvent event)
			throws IOException, InterruptedException {
		
		event.getFirewallManager().addRule(
			Rule.create().destination(role(ROLE)).ports(LISTEN_PORT,WEB_UI_PORT)
		);
		
		addStatement(event, call("configure_spark_master"));
	}

	@Override
	protected void afterConfigure(ClusterActionEvent event) throws IOException,
			InterruptedException {
	    Cluster cluster = event.getCluster();
	    Instance master = cluster.getInstanceMatching(role(ROLE));
	    String masterAddress = master.getPublicAddress().getHostName();
  	    System.out.printf("Spark master running at spark://%s:%s\n",
	        masterAddress, LISTEN_PORT);
	    System.out.printf("Spark UI running at http://%s:%s\n",
	        masterAddress, WEB_UI_PORT);
	}

}
