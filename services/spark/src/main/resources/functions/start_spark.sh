function start_druid() {

    ROLE=$1
    echo "Inside start_spark(), ROLE=$ROLE"

    # Start the appropriate role
    echo "Executing spark $ROLE..."
    case $ROLE in
        spark-master)
            # Run the master node
            cd /usr/local/spark-0.9.0-incubating
            nohup ./sbin/start-master.sh > /var/log/spark-master.log 2>&1 &
            ;;
        spark-worker)
            # Run the worker node
            cd /usr/local/spark-0.9.0-incubating
            nohup ./sbin/start-slave.sh > /var/log/spark-master.log 2>&1 &
            ;;
        *)
            echo $"Usage: $0 {spark-master|spark-worker}"
            exit 1
    esac

}
