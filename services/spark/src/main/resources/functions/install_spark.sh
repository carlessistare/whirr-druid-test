set -x

function install_spark() {
	
	apt-get install -y git
	git clone git://github.com/ijuma/junit_xml_listener.git /root/.sbt/staging/90b1b0889ba1299e38f2
	curl -kL -o /tmp/spark-0.9.0-incubating.tar.gz http://d3kbcqa49mib13.cloudfront.net/spark-0.9.0-incubating.tgz
	tar -C /usr/local/ -zxf /tmp/spark-0.9.0-incubating.tar.gz
	cd /usr/local/spark-0.9.0-incubating
	echo "export SPARK_LAUNCH_WITH_SCALA=1" > conf/spark-env.sh
	# sbt/sbt package
	# sbt/sbt assembly
	
}
