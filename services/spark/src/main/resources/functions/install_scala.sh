function install_scala_deb() {

  apt-get update
  wget http://www.scala-lang.org/files/archive/scala-2.10.4.deb
  dpkg -i scala-2.10.4.deb
  apt-get install libjansi-java
  apt-get -fy install
  echo `scala -version`
 }
 
 function install_scala_rpm() {
 	rpm -ivh --force http://www.scala-lang.org/files/archive/scala-2.10.4.rpm
 	
 	echo `scala -version`
  }
 
 
 function install_scala() {
	 if which dpkg &> /dev/null; then
	 	install_scala_deb
	 elif which rpm &> /dev/null; then
	 	install_scala_rpm
	 fi
 }
