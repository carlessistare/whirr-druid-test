# INSTALL jdk-7u45-linux-x64 in local machine (whirr launch-cluster needs it :( )

download http://download.oracle.com/otn-pub/java/jdk/7u45-b13/jdk-7u45-linux-x64.tar.gz

```bash
tar xvpf jdk-7u45-linux-x64.tar.gz
sudo unlink /usr/bin/java
sudo update-alternatives --install "/usr/bin/java" "java" "/usr/lib/jvm/jdk1.7.0_45/bin/java" 1041
sudo update-alternatives --config java
```

# Compile local whirr druid version

```bash
git clone git@bitbucket.org:carlessistare/whirr-druid-test.git
cd whirr-druid-test
mvn clean install -Dmaven.test.failure.ignore=true
```

# Prepare credentials

```bash
vim conf/credentials
```

# Create spec file

```bash
vim ~/events.spec
```
```json
[
{
  "schema": {
    "dataSource": "events",
    "aggregators" : [{
       "type" : "count",
       "name" : "count"
    }],
    "indexGranularity": "minute"
  },
  "config": {
    "maxRowsInMemory": 500000,
    "intermediatePersistPeriod": "PT10m"
  },
  "firehose": {
    "type": "kafka-0.7.2",
    "consumerProps": {
      "zk.connect": "localhost:2181",
      "zk.connectiontimeout.ms": "15000",
      "zk.sessiontimeout.ms": "15000",
      "zk.synctime.ms": "5000",
      "groupid": "druid-example",
      "fetch.size": "1048586",
      "autooffset.reset": "largest",
      "autocommit.enable": "false"
    },
    "feed": "events",
    "parser": {
      "timestampSpec": {
        "column": "timestamp"
      },
      "data": {
        "format": "json",
        "dimensions" : ["supportId","deviceId","event_type","value"]
      }
    }
  },
  "plumber": {
    "type": "realtime",
    "windowPeriod": "PT10m",
    "segmentGranularity": "hour",
    "basePersistDirectory": "\/tmp\/realtime\/basePersist",
    "rejectionPolicy": {
      "type": "none"
    }
  }
}
]
```

# Be sure druid properties are right

```bash
cat recipes/druid.properties
```

# Launh whirr

```bash
bin/whirr launch-cluster --config recipes/druid.properties

bin/whirr restart-services --config recipes/druid.properties

bin/whirr destroy-cluster --config recipes/druid.properties
```



# SSH into machine
download http://download.oracle.com/otn-pub/java/jdk/7/jdk-7-linux-x64.tar.gz


# INTALL JDK 7 Oracle

```bash
tar xvpf jdk-7-linux-x64.tar.gz
mv jdk1* /usr/lib/jvm/java-7-oracle
sudo unlink /usr/bin/java
sudo update-alternatives --install "/usr/bin/java" "java" "/usr/lib/jvm/java-7-oracle/bin/java" 1041
sudo update-alternatives --config java
```


# INSTALL KAFKA

```bash
wget http://archive.apache.org/dist/kafka/old_releases/kafka-0.7.2-incubating/kafka-0.7.2-incubating-src.tgz
tar -xvzf kafka-0.7.2-incubating-src.tgz
cd kafka-0.7.2-incubating-src
./sbt update
./sbt package
nohup bin/kafka-server-start.sh config/server.properties &

bin/kafka-console-producer.sh --zookeeper localhost:2181 --topic events
```


# RESTART SEVICES


# Prepare query

```bash
vim ~/query.json
```
```json
{
  "queryType": "groupBy",
  "dataSource": "events",
  "granularity": "day",
  "dimensions": ["deviceId"],
  "aggregations": [
    { "type": "count", "name": "count", "fieldName": "count" }
  ],
  "intervals": [ "2012-01-01T00:00:00.000/2015-01-03T00:00:00.000" ]
}
```

```bash
curl -X POST 'http://localhost:8083/druid/v2/?pretty' -H 'content-type: application/json' -d @~/query.json
```
